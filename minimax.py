import random


def minimax(board, depth, isMaximizing, games, cell_id, alpha, beta):
    # print(alpha, beta)
    if games[cell_id].checkWhichMarkWon(games[cell_id].bot):
        # print("WIN")
        return 1000, 1000
    elif games[cell_id].checkWhichMarkWon(games[cell_id].player):
        # print("LOST")
        return -1000, -1000
    elif games[cell_id].checkDraw():
        # print("DRAW")
        return 0, 0

    keys = list(games[cell_id].board.keys())
    #random.shuffle(keys)
    #print(keys)
    if isMaximizing:
        bestScore = -8000
        for key in keys:
            if games[cell_id].board[key] == ' ':
                points = mark_game(games[cell_id].board)

                if points <= beta:
                    # print(alpha, beta)
                    return alpha, beta
                games[cell_id].board[key] = games[cell_id].bot
                new_alpha, new_beta = minimax(board, depth + 1, False, games, key - 1, alpha, beta)
                beta = max(beta, new_alpha)
                games[cell_id].board[key] = ' '

                if alpha <= beta:
                    # print(alpha, beta)
                    return alpha, beta
                # if score > bestScore:
                #     bestScore = score
                #     if score == 1:
                #         print("WIN " + str(depth) + " " + str(key))
                #         return bestScore
        return alpha, beta

    else:
        bestScore = 8000
        for key in keys:
            if games[cell_id].board[key] == ' ':
                points = mark_game(games[cell_id].board)

                if points >= alpha:
                    # print(alpha, beta)
                    return alpha, beta
                games[cell_id].board[key] = games[cell_id].player
                new_alpha, new_beta = minimax(board, depth + 1, True, games, key - 1, alpha, beta)
                alpha = min(alpha, new_beta)
                games[cell_id].board[key] = ' '

                if alpha >= beta:
                    # print(alpha, beta)
                    return alpha, beta
                # if score < bestScore:
                #     bestScore = score
        return alpha, beta


def mark_game(board):
    points = 0
    my_board = [[board[1], board[2], board[3]], [board[4], board[5], board[6]], [board[7], board[8], board[9]]]
    for row in my_board:
        if row.count('X') == 2:
            points += 20
        if row.count('O') == 2:
            points -= 20
    for i in range(3):
        col = [my_board[0][i]] + [my_board[1][i]] + [my_board[2][i]]
        if col.count('X') == 2:
            points += 20
        if col.count('O') == 2:
            points -= 20

    diag1 = [my_board[0][0]] + [my_board[1][1]] + [my_board[2][2]]
    diag2 = [my_board[0][2]] + [my_board[1][1]] + [my_board[2][1]]

    if diag1.count('X') == 2:
        points += 20
    if diag2.count('X') == 2:
        points += 20

    if diag1.count('O') == 2:
        points -= 20
    if diag2.count('O') == 2:
        points -= 20

    if my_board[1][1] == 'X':
        points += 10
    if my_board[1][1] == 'O':
        points -= 10

    corners = [my_board[0][0]] + [my_board[0][2]] + [my_board[2][0]] + [my_board[2][2]]
    xs = corners.count('X')
    os = corners.count('O')
    points += xs * 5
    points -= os * 5
    # print("P:" + str(points))
    return points