import minimax


class Game:

    isLocked = False


    player = 'O'
    bot = 'X'

    def __init__(self, id, padding_x, padding_y, games):
        self.id = id
        self.padding_x = padding_x
        self.padding_y = padding_y
        self.board = {1: ' ', 2: ' ', 3: ' ',
                 4: ' ', 5: ' ', 6: ' ',
                 7: ' ', 8: ' ', 9: ' '}
        self.games = games

    def checkForWin(self):
        if self.board[1] == self.board[2] == self.board[3] and self.board[1] != ' ':
            return True
        elif self.board[4] == self.board[5] == self.board[6] and self.board[4] != ' ':
            return True
        elif self.board[7] == self.board[8] == self.board[9] and self.board[7] != ' ':
            return True
        elif self.board[1] == self.board[4] == self.board[7] and self.board[1] != ' ':
            return True
        elif self.board[2] == self.board[5] == self.board[8] and self.board[2] != ' ':
            return True
        elif self.board[3] == self.board[6] == self.board[9] and self.board[3] != ' ':
            return True
        elif self.board[1] == self.board[5] == self.board[9] and self.board[1] != ' ':
            return True
        elif self.board[7] == self.board[5] == self.board[3] and self.board[7] != ' ':
            return True
        else:
            return False

    def checkWhichMarkWon(self, mark):
        if self.board[1] == self.board[2] == self.board[3] and self.board[1] == mark:
            return True
        elif self.board[4] == self.board[5] == self.board[6] and self.board[4] == mark:
            return True
        elif self.board[7] == self.board[8] == self.board[9] and self.board[7] == mark:
            return True
        elif self.board[1] == self.board[4] == self.board[7] and self.board[1] == mark:
            return True
        elif self.board[2] == self.board[5] == self.board[8] and self.board[2] == mark:
            return True
        elif self.board[3] == self.board[6] == self.board[9] and self.board[3] == mark:
            return True
        elif self.board[1] == self.board[5] == self.board[9] and self.board[1] == mark:
            return True
        elif self.board[7] == self.board[5] == self.board[3] and self.board[7] == mark:
            return True
        else:
            return False

    def checkDraw(self):
        for key in self.board.keys():
            if self.board[key] == ' ':
                return False
        return True

    def compMove(self, cell):
        print("COMP_MOVE " + str(cell - 1))
        bestScore = -800
        bestMove = 0
        # print("==================")
        for key in self.board.keys():
            if self.games[cell-1].board[key] == ' ':
                self.games[cell-1].board[key] = self.bot
                alpha, beta = minimax.minimax(self.board, 0, False, self.games, cell - 1, 100000, -100000)
                self.games[cell-1].board[key] = ' '

                if alpha > bestScore:
                    bestScore = alpha
                    bestMove = key
                # print(bestMove, score)

        #self.insertLetter(self.bot, bestMove)
        return bestMove

    # def minimax(self, board, depth, isMaximizing):
    #     if self.checkWhichMarkWon(self.bot):
    #         return 1
    #     elif self.checkWhichMarkWon(self.player):
    #         return -1
    #     elif self.checkDraw():
    #         return 0
    #
    #     if isMaximizing:
    #         bestScore = -800
    #         for key in board.keys():
    #             if board[key] == ' ':
    #                 board[key] = self.bot
    #                 score = self.minimax(board, depth + 1, False)
    #                 board[key] = ' '
    #                 if score > bestScore:
    #                     bestScore = score
    #         return bestScore
    #
    #     else:
    #         bestScore = 800
    #         for key in board.keys():
    #             if board[key] == ' ':
    #                 board[key] = self.player
    #                 score = self.minimax(board, depth + 1, True)
    #                 board[key] = ' '
    #                 if score < bestScore:
    #                     bestScore = score
    #         return bestScore

    def insertLetter(self, letter, position):
        if self.spaceIsFree(position):
            self.board[position] = letter
            if self.checkDraw():
                self.isLocked = True
                print("Ничья!!")
            if self.checkForWin():
                if letter == 'X':
                    self.isLocked = True
                    print("Компьютер победил!")
                else:
                    self.isLocked = True
                    print("Игрок победил!")

            return True
        else:
            print("Сюда нельзя ходить")
            return False

    def spaceIsFree(self, position):
        if position == 0:
            return False
        if self.board[position] == ' ':
            return True
        else:
            return False
