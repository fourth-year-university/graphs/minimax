import sys

import pygame
from game import Game

CELL_SIZE = 50
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
LINE_WIDTH = 5
GREEN = (0, 255, 0)
RED = (255, 0, 0)

pygame.init()
SCREEN = pygame.display.set_mode((CELL_SIZE * 5 * 3, CELL_SIZE * 5 * 3))
CLOCK = pygame.time.Clock()
SCREEN.fill(BLACK)
FONT = pygame.font.Font(pygame.font.get_default_font(), 36)


def drawScreen():
    SCREEN.fill(BLACK)

    for game in games:
        for x in range(1, 4):
            for y in range(1, 4):
                rect = pygame.Rect(x * CELL_SIZE + game.padding_x, y * CELL_SIZE + game.padding_y, CELL_SIZE, CELL_SIZE)
                pygame.draw.rect(SCREEN, WHITE, rect, LINE_WIDTH)
        for key in game.board.keys():
            if game.board[key] == game.bot:
                x = (key - 1) % 3 + 1
                y = (key - 1) // 3 + 1
                text_surface = FONT.render(game.bot, True, RED)
                SCREEN.blit(text_surface,
                            dest=(x * CELL_SIZE + CELL_SIZE * 0.2 + game.padding_x,
                                  y * CELL_SIZE + CELL_SIZE * 0.2 + game.padding_y))
            if game.board[key] == game.player:
                x = (key - 1) % 3 + 1
                y = (key - 1) // 3 + 1
                text_surface = FONT.render(game.player, True, GREEN)
                SCREEN.blit(text_surface,
                            dest=(x * CELL_SIZE + CELL_SIZE * 0.2 + game.padding_x,
                                  y * CELL_SIZE + CELL_SIZE * 0.2 + game.padding_y))


games = []

for i in range(9):
    games.append(Game(i, CELL_SIZE * 3 * (i % 3) + CELL_SIZE * (i % 3), CELL_SIZE * 3 * (i // 3) + CELL_SIZE * (i // 3), games))

best_move = 5
while True:
    drawScreen()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        if event.type == pygame.MOUSEBUTTONUP:
            pos = pygame.mouse.get_pos()
            for game in games:
                x = (pos[0] - game.padding_x) // CELL_SIZE - 1
                y = (pos[1] - game.padding_y) // CELL_SIZE - 1

                cell = y * 3 + x + 1
                if (pos[0] - game.padding_x) < CELL_SIZE or (pos[1] - game.padding_y) < CELL_SIZE or (pos[0] - game.padding_x) > CELL_SIZE * 4 or (pos[1] - game.padding_y) > CELL_SIZE * 4:
                    continue
                if game.id + 1 != best_move:
                    print("Неверное поле")
                    continue
                if game.insertLetter(game.player, cell):
                    print(game.board)
                    best_move = game.compMove(cell)
                    games[cell - 1].insertLetter(games[cell - 1].bot, best_move)

    pygame.display.update()
